#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

// Define the struct
struct MyStruct {
    float float1;
    float float2;
    float float3;
    float float4;
    int8_t intVar;
};

int main() {
    // Define the key that matches the key used in the Python script
    key_t shm_key = 1929590349 ;

    // Create or open the shared memory segment
    int shm_id = shmget(shm_key, sizeof(struct MyStruct), 0666);
    if (shm_id == -1) {
        perror("shmget");
        exit(1);
    }

    // Attach the shared memory segment to this process
    struct MyStruct *shm_ptr = (struct MyStruct *)shmat(shm_id, NULL, 0);
    if ((intptr_t)shm_ptr == -1) {
        perror("shmat");
        exit(1);
    }

    // Read the data from the shared memory
    printf("Float 1: %f\n", shm_ptr->float1);
    printf("Float 2: %f\n", shm_ptr->float2);
    printf("Float 3: %f\n", shm_ptr->float3);
    printf("Float 4: %f\n", shm_ptr->float4);
    printf("IntVar: %d\n", shm_ptr->intVar);

    // Detach the shared memory segment
    shmdt(shm_ptr);

    return 0;
}