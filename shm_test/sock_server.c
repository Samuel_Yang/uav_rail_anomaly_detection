#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

int main(int argc, char* argv[]){
    if(argc<3){
        printf("input should be ./sock_server <IP> <PORT>");
    }
    int server_socket, client_socket;

    struct sockaddr_in server_addr, client_addr;
    socklen_t client_len;
    int addr = atoi(argv[1]);
    int port = atoi(argv[2]);
    
    char command[1024];

    // Create socket
    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket == -1) {
        perror("Socket creation failed");
        exit(1);
    }

    // Configure server address
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port); // Modify with your desired port
    server_addr.sin_addr.s_addr = INADDR_ANY;

    // Bind
    if (bind(server_socket, (struct sockaddr*)&server_addr, sizeof(server_addr)) == -1) {
        perror("Binding failed");
        close(server_socket);
        exit(1);
    }

    // Listen
    if (listen(server_socket, 5) == -1) {
        perror("Listening failed");
        close(server_socket);
        exit(1);
    }

    printf("Server listening on port %d...\n",port);

    // Accept client connection
    client_len = sizeof(client_addr);
    client_socket = accept(server_socket, (struct sockaddr*)&client_addr, &client_len);
    if (client_socket == -1) {
        perror("Accepting client failed");
        close(server_socket);
        exit(1);
    }

    while (1) {
        printf("Enter a command to send to the client (or 'exit' to quit): ");
        fgets(command, sizeof(command), stdin);

        // Send the command to the client
        send(client_socket, command, strlen(command), 0);

        // Check for exit condition
        if (strncmp(command, "exit", 4) == 0) {
            break;
        }
    }

    // Close sockets
    close(client_socket);
    close(server_socket);

    return 0;
}