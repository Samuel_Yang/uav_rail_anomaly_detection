#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define SHM_KEY 12345  // Change this key as needed

// Define the struct
struct MyStruct {
    float float1;
    float float2;
    float float3;
    float float4;
    int intVar;
};

int main() {
    // Create or open the shared memory segment
    key_t key = ftok("./predict", 's');
    printf("key = %d\n",key);
    int shm_id = shmget(key, (sizeof(struct MyStruct)), IPC_CREAT | 0666);
    if (shm_id == -1) {
        perror("shmget");
        exit(1);
    }

    // Attach the shared memory segment to this process
    struct MyStruct *shm_ptr = (struct MyStruct *)shmat(shm_id, NULL, 0);
    if ((intptr_t)shm_ptr == -1) {
        perror("shmat");
        exit(1);
    }

    // Fill the struct with some sample data
    shm_ptr->float1 = 1.23;
    shm_ptr->float2 = 4.56;
    shm_ptr->float3 = 7.89;
    shm_ptr->float4 = 0.12;
    shm_ptr->intVar = 42;

    // Detach the shared memory segment
    shmdt(shm_ptr);

    return 0;
}
