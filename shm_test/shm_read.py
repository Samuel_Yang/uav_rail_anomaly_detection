import ctypes
from ctypes import cdll
# Define the structure that matches the one in your C code
class MyStruct(ctypes.Structure):
    _fields_ = [("float1", ctypes.c_float),
                ("float2", ctypes.c_float),
                ("float3", ctypes.c_float),
                ("float4", ctypes.c_float),
                ("int_value", ctypes.c_int)]

# Define the key value
key = 0x7303364d

# Load the shared library (C code) that created the shared memory
# Replace 'shared_lib.so' with the actual name of your shared library on Linux/macOS
# Replace 'shared_lib.dll' with the actual name of your shared library on Windows

# On Linux/macOS
shared_lib = ctypes.CDLL(None)

# Attach to the shared memory segment
shm_id = shared_lib.shmget(key, 0, 0)

if shm_id == -1:
    print("Failed to access shared memory.")
else:
    # Attach to the shared memory segment
    shm_ptr = shared_lib.shmat(shm_id, 0, 0)

    if shm_ptr == -1:
        print("Failed to attach to shared memory.")
    else:
        # Read the data from shared memory
        my_struct = MyStruct()
        ctypes.memmove(ctypes.byref(my_struct), shm_ptr, ctypes.sizeof(my_struct))
        
        # Access individual fields in the structure
        print("Float 1:", my_struct.float1)
        print("Float 2:", my_struct.float2)
        print("Float 3:", my_struct.float3)
        print("Float 4:", my_struct.float4)
        print("Int Value:", my_struct.int_value)

        # Detach from the shared memory segment
        shared_lib.shmdt(shm_ptr)
